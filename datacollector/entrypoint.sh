#!/bin/bash

cd /app/datacollector
source server_urls.sh
export ORD="${MY_POD_NAME//[!0-9]/}"
export MASTODON_SERVER_URL=${serverUrls[$ORD]}
printenv > /etc/environment
/usr/local/bin/python -m datacollector.main
cron -f
