import yaml
import os


class _Config:
    def __init__(self) -> None:
        with open(
            os.path.join(
                os.path.dirname(os.path.dirname(__file__)), "config.yaml"
            )
        ) as yaml_config_file:
            self.config = yaml.load(yaml_config_file, Loader=yaml.SafeLoader)

    def __getattr__(self, name):
        return self.config[name] or None


config = _Config()
