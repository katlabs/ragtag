from .config import config
from mastodon import Mastodon
from pika import PlainCredentials, ConnectionParameters, BlockingConnection
from database_client import DatabaseClient
import os
import re
from sys import stdout
from urllib.parse import urlparse, quote
import logging
import logging.handlers


class Datacollector:
    def __init__(self) -> None:
        self.__setup_logger()
        self.logger.info(
            f"Datacollector started for server {os.getenv('MASTODON_SERVER_URL')}"
        )
        self.__setup_database()
        self.__setup_rabbitmq()

    def __setup_logger(self):
        self.logger = logging.getLogger(config.app_name)
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            "%(asctime)s | %(name)s | %(levelname)s: %(message)s"
        )

        consolehandler = logging.StreamHandler(stdout)
        consolehandler.setFormatter(formatter)
        self.logger.addHandler(consolehandler)

    def __setup_database(self):
        api_password = os.getenv("DATACOLLECTOR_PASSWORD", None)
        if not api_password:
            raise Exception("No api password provided in environment.")

        self.client = DatabaseClient(
            config.api_base_url, config.app_name, api_password
        )

    def __setup_rabbitmq(self):
        rabbit_user = os.getenv("RABBITMQ_DEFAULT_USER", None)
        if not rabbit_user:
            raise Exception("No rabbitmq password provided in environment.")
        rabbit_pw = os.getenv("RABBITMQ_DEFAULT_PASS", None)
        if not rabbit_pw:
            raise Exception("No rabbitmq password provided in environment.")
        rq_credentials = PlainCredentials(rabbit_user, rabbit_pw)
        rq_con_params = ConnectionParameters(
            host="rabbitmq", credentials=rq_credentials
        )
        try:
            self.rq_connection = BlockingConnection(rq_con_params)
            self.rq_channel = self.rq_connection.channel()
        except:
            raise Exception(f"Dataanalyzer failed to connect to rabbitmq.")

        self.rq_channel.queue_declare("links")

    def create_mastodon_server(self, server_url: str):
        db_server = self.client.get_server(quote(server_url, safe=""))

        if not db_server:
            self.logger.debug(
                f"Server {server_url} not in database, adding it..."
            )
            client_id, client_secret = Mastodon.create_app(
                config.mastodon_client_name,
                scopes=["read"],
                api_base_url=server_url,
            )
            db_server = {
                "url": server_url,
                "client_id": client_id,
                "client_secret": client_secret,
            }
            new_link = self.client.create_server(db_server)
            if not new_link:
                self.logger.error(
                    f"Failed to add new Mastodon server {server_url}, to the database."
                )
                self.logger.error(f"Aborting datacollection for {server_url}.")
                raise Exception(
                    f"Server {server_url} could not be added to the database."
                )
            else:
                self.logger.debug(f"Added new server {server_url}.")
        else:
            self.logger.debug(f"Found server {server_url}.")

        mastodon_server = Mastodon(
            api_base_url=server_url,
            client_id=db_server["client_id"],
            client_secret=db_server["client_secret"],
        )
        return mastodon_server

    def create_or_update_link(self, link_url: str, tag: str, toot_url: str):
        link_url_safe = quote(link_url, safe="")
        db_link = self.client.get_link(link_url_safe)

        if not db_link:
            self.logger.debug(f"Link not found in database, adding it...")
            db_link = {
                "url": link_url,
                "tags": [{"word": tag}],
                "toots": [{"url": toot_url}],
            }
            new_link = self.client.create_link(db_link)
            if not new_link:
                self.logger.error(
                    f"Failed to create link {link_url}, skipping..."
                )
                return "failed"
            else:
                self.logger.debug(f"Link {link_url} created.")
                return "created"
        else:
            self.logger.debug(f"Link already in database, updating it...")
            db_link["tags"].append({"word": tag})
            db_link["toots"].append({"url": toot_url})
            new_link = self.client.update_link(link_url_safe, db_link)
            if not new_link:
                self.logger.error(
                    f"Failed to update link {link_url}, skipping..."
                )
                return "failed"
            else:
                self.logger.debug(f"Link {link_url} updated.")
                return "updated"

    def parse_links(self, toot_content: str):
        filtered_domains = config.filtered_domains
        regex = r"<a\s+(?:[^>]*?\s+)?href=[\"\'](.*?)[\"\']"
        urls = re.findall(regex, toot_content)
        return [
            x.rstrip("/")
            for x in urls
            if urlparse(x).hostname not in filtered_domains
        ]

    def collect_links(self):
        link_count = 0
        tag_count = 0
        toot_count = 0
        failed_links = 0
        server_url = os.getenv("MASTODON_SERVER_URL")
        if not server_url:
            raise Exception(f"Mastodon server url not provided")
        server = self.create_mastodon_server(server_url)
        trending_tags = server.trending_tags(limit=10)
        for tag in trending_tags:
            tag_count += 1
            timeline = server.timeline_hashtag(hashtag=tag.name, local=True)
            for toot in timeline:
                toot_count += 1
                links = self.parse_links(toot.content)
                for link in links:
                    link_count += 1
                    status = self.create_or_update_link(
                        link, tag.name, toot.url
                    )
                    if status == "failed":
                        failed_links += 1
                    elif status == "created":
                        self.rq_channel.basic_publish("", "links", link)

        self.logger.info(
            f"{server_url} processed {link_count} links from {tag_count} tags, {toot_count} toots."
        )
        self.logger.info(
            f"Failed to create or update {failed_links}/{link_count} links."
        )
        self.rq_connection.close()


if __name__ == "__main__":
    datacollector = Datacollector()
    datacollector.collect_links()
