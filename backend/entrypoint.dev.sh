#!/bin/sh

echo "Migrating database"
python manage.py makemigrations
python manage.py migrate
echo "Collecting static files"
python manage.py collectstatic --noinput
echo "Creating admin"
python manage.py createsuperuser --noinput
echo "Creating datacollector"
DJANGO_SUPERUSER_USERNAME=datacollector \
    DJANGO_SUPERUSER_PASSWORD=$DATACOLLECTOR_PASSWORD \
    DJANGO_SUPERUSER_EMAIL=datacollector@example.com \
    python manage.py createsuperuser --noinput
echo "Creating dataanalyzer"
DJANGO_SUPERUSER_USERNAME=dataanalyzer \
    DJANGO_SUPERUSER_PASSWORD=$DATAANALYZER_PASSWORD \
    DJANGO_SUPERUSER_EMAIL=dataanalyzer@example.com \
    python manage.py createsuperuser --noinput
echo "Starting server"
python manage.py runserver 0.0.0.0:8000
exec "$@"
