from .base import *

DEBUG = True

SECRET_KEY = "insecure-testing-key-not-for-production"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "test.db",
    }
}
