import os
from .base import *

DEBUG = True
SECRET_KEY = env(
    "SECRET_KEY", default="insecure-development-key-not-for-production"
)
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": env("MYSQL_DATABASE", default="mariadb"),
        "USER": env("MYSQL_USER", default="mariadb"),
        "PASSWORD": env("MYSQL_PASSWORD", default="mariadb"),
        "HOST": env("DB_HOST_NAME", default="db"),
        "PORT": 3306,
        "OPTIONS": {
            "init_command": "SET sql_mode='STRICT_TRANS_TABLES', innodb_strict_mode=1",
        },
    }
}
