from django.urls import path
from .views import LatestArticlesRSSFeed, LatestArticlesAtomFeed


urlpatterns = [
    path("rss/", LatestArticlesRSSFeed(), name="feed-rss"),
    path("atom/", LatestArticlesAtomFeed(), name="feed-atom"),
]
