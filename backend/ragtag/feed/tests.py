from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
import logging

logger = logging.getLogger("ragtag")


class TestGetFeeds(APITestCase):
    fixtures = ["fake_articles"]

    def setUp(self):
        self.rss_url = reverse("feed-rss")
        self.atom_url = reverse("feed-atom")
        self.client = APIClient()

    def test_get_rss_feed(self):
        response = self.client.get(self.rss_url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(
            response.get("Content-Type"), "application/rss+xml; charset=utf-8"
        )
        self.assertGreater(int(response.get("Content-Length")), 0)

    def test_get_atom_feed(self):
        response = self.client.get(self.atom_url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(
            response.get("Content-Type"), "application/atom+xml; charset=utf-8"
        )
        self.assertGreater(int(response.get("Content-Length")), 0)
