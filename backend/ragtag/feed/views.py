from django.contrib.syndication.views import Feed
from ..articles.models import Article
from django.utils.feedgenerator import Atom1Feed


class LatestArticlesRSSFeed(Feed):
    title = "RagTag - Latest Articles Feed"
    link = "/feed/"
    description = "Latest articles found on RagTag."

    def items(self):
        return Article.objects.order_by("-fetch_date")

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.text

    def item_pubdate(self, item):
        return item.publish_date

    def item_link(self, item):
        return item.link.url


class LatestArticlesAtomFeed(LatestArticlesRSSFeed):
    feed_type = Atom1Feed
    subtitle = LatestArticlesRSSFeed.description
