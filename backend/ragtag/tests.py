from django.test import TestCase
import logging
from re import search

from django.urls import reverse

logger = logging.getLogger("ragtag")


class TestMetrics(TestCase):
    def test_get_metrics(self):
        response = self.client.get("/metrics")
        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            response.content.decode().find(
                'django_http_requests_total_by_method_total{method="GET"} 1.0'
            )
        )


class TestHealthcheck(TestCase):
    def test_get_health(self):
        response = self.client.get(reverse("health-check"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode(), "I'm healthy")
