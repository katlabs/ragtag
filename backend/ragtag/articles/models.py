from django.db import models


class Server(models.Model):
    url = models.URLField(unique=True)
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    client_id = models.CharField(max_length=500)
    client_secret = models.CharField(max_length=500)

    class Meta:
        ordering = ["url"]


class Tag(models.Model):
    word = models.CharField(max_length=100)

    class Meta:
        ordering = ["word"]


class Toot(models.Model):
    url = models.URLField()

    class Meta:
        ordering = ["url"]


class Link(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    url = models.URLField(max_length=2048, unique=True)
    tags = models.ManyToManyField(Tag)
    toots = models.ManyToManyField(Toot)

    class Meta:
        ordering = ["-last_modified"]


class Article(models.Model):
    title = models.CharField(max_length=500)
    authors = models.JSONField(default=list)
    publish_date = models.DateTimeField(null=True)
    fetch_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    summary = models.CharField(max_length=500)
    keywords = models.JSONField(default=list)
    link = models.OneToOneField(
        Link, null=True, unique=True, on_delete=models.SET_NULL
    )

    class Meta:
        ordering = ["-fetch_date"]
