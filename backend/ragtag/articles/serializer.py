from rest_framework import serializers
from .models import Article, Link, Server, Tag, Toot
import logging

logger = logging.getLogger("ragtag")


class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Server
        lookup_field = "url"
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class TootSerializer(serializers.ModelSerializer):
    class Meta:
        model = Toot
        fields = "__all__"


class LinkSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    toots = TootSerializer(many=True)

    class Meta:
        model = Link
        fields = "__all__"

    def create(self, validated_data):
        tags = validated_data.pop("tags", [])
        toots = validated_data.pop("toots", [])
        link = Link.objects.create(**validated_data)
        for tag_data in tags:
            tag, _ = Tag.objects.get_or_create(**tag_data)
            link.tags.add(tag)
        for toot_data in toots:
            toot, _ = Toot.objects.get_or_create(**toot_data)
            link.toots.add(toot)
        return link

    def update(self, instance, validated_data):
        tags = validated_data.pop("tags", [])
        toots = validated_data.pop("toots", [])
        tag_objs = []
        toot_objs = []
        for tag_data in tags:
            tag, _ = Tag.objects.get_or_create(**tag_data)
            tag_objs.append(tag)
        instance.tags.set(tag_objs)
        for toot_data in toots:
            toot, _ = Toot.objects.get_or_create(**toot_data)
            toot_objs.append(toot)
        instance.toots.set(toot_objs)
        instance.last_modified
        return instance


class ArticleCreateSerializer(serializers.ModelSerializer):
    link = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Link.objects.all()
    )

    class Meta:
        model = Article
        fields = "__all__"


class ArticleViewSerializer(serializers.ModelSerializer):
    link = LinkSerializer(many=False, read_only=True)

    class Meta:
        model = Article
        fields = "__all__"
