from django.urls import path
from .views import (
    LinkDetail,
    LinkList,
    ServerList,
    ServerDetail,
    ArticleList,
    ArticleCreate,
    ArticleUpdate,
)

urlpatterns = [
    path("articles/list/", ArticleList.as_view(), name="article-list"),
    path("articles/", ArticleCreate.as_view(), name="article-create"),
    path("article/<int:pk>/", ArticleUpdate.as_view(), name="article-update"),
    path("servers/", ServerList.as_view(), name="server-list"),
    path("server/<path:url>/", ServerDetail.as_view(), name="server-detail"),
    path("links/", LinkList.as_view(), name="link-list"),
    path("link/<path:url>/", LinkDetail.as_view(), name="link-detail"),
]
