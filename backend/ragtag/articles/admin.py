from django.contrib import admin
from .models import Article, Server, Link, Tag, Toot

admin.site.register(Article)
admin.site.register(Server)
admin.site.register(Link)
admin.site.register(Tag)
admin.site.register(Toot)
