from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
import logging
from datetime import datetime

logger = logging.getLogger("ragtag")


def setup_client():
    username = "dummyuser"
    password = "dummypass"
    user = User.objects.create_superuser(username=username)
    user.set_password(password)
    user.save()
    client = APIClient()
    return username, password, client


class TestPostArticle(APITestCase):
    fixtures = ["fake_articles"]

    def setUp(self):
        self.username, self.password, self.client = setup_client()
        self.url = reverse("article-create")
        self.payload = {
            "title": "Test Article 6",
            "authors": ["John Sung", "Amy Cocoa"],
            "summary": "Test article summary",
            "text": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh viverra non semper suscipit posuere a pede. Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.Morbi in sem quis dui placerat ornare. Pellentesque odio nisi euismod in pharetra a ultricies in diam. Sed arcu. Cras consequat. Praesent dapibus neque id cursus faucibus tortor neque egestas auguae eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi tincidunt quis accumsan porttitor facilisis luctus metus. Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam gravida non commodo a sodales sit amet nisi.",
            "publish_date": "2023-07-09T17:53:25-04:00",
            "keywords": ["boring", "art"],
            "link": 6,
        }

    def test_authenticated_post_article(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.post(
            self.url,
            data=self.payload,
            format="json",
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_unauthenticated_post_article(self):
        self.client.logout()
        response = self.client.post(self.url, data=self.payload, format="json")
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)


class TestGetArticles(APITestCase):
    fixtures = ["fake_articles"]

    def setUp(self):
        self.username, self.password, self.client = setup_client()
        self.url = reverse("article-list")

    def test_get_articles(self):
        expected_first_article_title = "Article 1 Title"
        response = self.client.get(self.url, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(
            response.json()[0]["title"], expected_first_article_title
        )


class TestCreateUpdateServer(APITestCase):
    def setUp(self) -> None:
        self.username, self.password, self.client = setup_client()
        self.client.login(username=self.username, password=self.password)
        self.payload = {
            "url": "https://www.server.com",
            "client_id": "testclientid-5&*69685%^&$!@#*",
            "client_secret": "testclientsecret%@!@#$&",
        }

    def test_create_update_server(self):
        # Create a server
        response = self.client.post(
            reverse("server-list"), data=self.payload, format="json"
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        # Update the server
        update_server = response.json()
        access_time = datetime.now().isoformat()
        new_secret = "newtestclientsecret&$*%98&%^96gJG"

        update_server["client_secret"] = new_secret
        url = reverse("server-detail", args=[self.payload["url"]])
        response = self.client.put(
            url,
            data=update_server,
            format="json",
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(new_secret, response.json()["client_secret"])


class TestCreateUpdateLink(APITestCase):
    def setUp(self) -> None:
        self.username, self.password, self.client = setup_client()
        self.client.login(username=self.username, password=self.password)
        self.url = reverse("link-list")
        self.payload = {
            "processed": False,
            "url": "https://www.example.org/watch?v=example",
            "tags": [
                {"word": "tag1"},
                {"word": "tag2"},
            ],
            "toots": [
                {"url": "https://example.com/toot1"},
                {"url": "https://example.com/toot2"},
            ],
        }

    def test_create_update_link(self):
        # Create a link
        response = self.client.post(self.url, data=self.payload, format="json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        # Get the link
        url = reverse("link-detail", args=[self.payload["url"]])
        response = self.client.get(url, format="json")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(self.payload["url"], response.json()["url"])

        # Update the link
        update_link = self.payload
        update_link["tags"].append({"word": "tag3"})
        update_link["toots"].append({"url": "https://example.com/toot3"})
        response = self.client.put(
            url,
            data=update_link,
            format="json",
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIn({"id": 3, "word": "tag3"}, response.json()["tags"])
        self.assertIn(
            {"id": 3, "url": "https://example.com/toot3"},
            response.json()["toots"],
        )
