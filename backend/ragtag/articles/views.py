from rest_framework import permissions
from rest_framework.generics import (
    CreateAPIView,
    RetrieveUpdateDestroyAPIView,
    ListAPIView,
    ListCreateAPIView,
)
from .models import Article, Link, Server
from .serializer import (
    ArticleCreateSerializer,
    ArticleViewSerializer,
    LinkSerializer,
    ServerSerializer,
)
import logging

logger = logging.getLogger("ragtag")


class ArticleList(ListAPIView):
    """
    List all articles
    """

    queryset = Article.objects.all()
    serializer_class = ArticleViewSerializer


class ArticleCreate(CreateAPIView):
    """
    Create an article
    """

    queryset = Article.objects.all()
    serializer_class = ArticleCreateSerializer
    permission_classes = [permissions.IsAuthenticated]


class ArticleUpdate(RetrieveUpdateDestroyAPIView):
    """
    Retrieve, put, or delete an article
    """

    queryset = Article.objects.all()
    serializer_class = ArticleViewSerializer
    permission_classes = [permissions.IsAuthenticated]


class ServerList(ListCreateAPIView):
    """
    List all servers
    """

    queryset = Server.objects.all()
    serializer_class = ServerSerializer
    permission_classes = [permissions.IsAuthenticated]


class ServerDetail(RetrieveUpdateDestroyAPIView):
    """
    Retrieve, put, or delete a server
    """

    queryset = Server.objects.all()
    serializer_class = ServerSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = "url"


class LinkList(ListCreateAPIView):
    """
    List all servers
    """

    queryset = Link.objects.all()
    serializer_class = LinkSerializer
    permission_classes = [permissions.IsAuthenticated]


class LinkDetail(RetrieveUpdateDestroyAPIView):
    """
    Retrieve, put, or delete a server
    """

    queryset = Link.objects.all()
    serializer_class = LinkSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = "url"
