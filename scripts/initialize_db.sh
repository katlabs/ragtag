#!/bin/bash

source .env

helm install db oci://registry-1.docker.io/bitnamicharts/mariadb -n ragtag --set nameOverride=db,auth.database=$MYSQL_DATABASE,auth.rootPassword=$MYSQL_ROOT_PASSWORD,auth.username=$MYSQL_USER,auth.password=$MYSQL_PASSWORD
