#!/bin/bash

source .env

helm install db oci://registry-1.docker.io/bitnamicharts/mariadb -n ragtag --set nameOverride=db,auth.rootPassword=$MYSQL_ROOT_PASSWORD
