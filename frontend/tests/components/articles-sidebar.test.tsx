import { h } from "preact";
import mocks from "../__mocks__/dataMocks";
import Sidebar from "../../src/components/articles-sidebar";
// See: https://github.com/preactjs/enzyme-adapter-preact-pure
import { shallow, mount } from "enzyme";

describe("Test of the Sidebar", () => {
  test("Sidebar renders test articles", () => {
    const context = shallow(<Sidebar articles={mocks.mock_articles} />);
    const nav = context.find("nav");
    expect(nav.children()).toHaveLength(2);
    const link1 = nav.childAt(0);
    expect(link1.find("div.title").text()).toBe("Test Article 1");
    const link2 = nav.childAt(1);
    expect(link2.find("div.title").text()).toBe("Test Article 2");
  });
  test("Sidebar dropdown changes article limit", () => {
    let limit = 10;
    const mockSetLimit = () => {
      limit = 50;
    };
    const context = mount(
      <Sidebar
        articles={mocks.mock_articles}
        total={25}
        limit={limit}
        setLimit={mockSetLimit}
      />
    );
    const dropdown = context.find("#limit-dropdown");
    dropdown.simulate("change", 50);
    expect(limit).toEqual(50);
  });
});
