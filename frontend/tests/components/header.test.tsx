import { h } from "preact";
import Header from "../../src/components/header";
// See: https://github.com/preactjs/enzyme-adapter-preact-pure
import { shallow } from "enzyme";

describe("Initial Test of the Header", () => {
  test("Header renders 1 link item and 1 subheader item", () => {
    const context = shallow(<Header />);
    expect(context.children()).toHaveLength(2);
    expect(context.find("#header-text").find("span").first().text()).toBe(
      "Community-Curated News from the Fediverse"
    );
  });
});
