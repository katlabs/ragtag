import { h } from "preact";
import mocks from "../__mocks__/dataMocks";
import ArticleView from "../../src/components/article-view";
// See: https://github.com/preactjs/enzyme-adapter-preact-pure
import { shallow } from "enzyme";

describe("Test of the Article View", () => {
  test("Article View renders a childless div when there is no article", () => {
    const context = shallow(<ArticleView />);
    expect(context.find("div")).toBeTruthy();
    expect(context.find("div").children().length).toBe(0);
  });
  test("Article View renders a test article", () => {
    const context = shallow(<ArticleView article={mocks.mock_articles[0]} />);
    expect(context.find("h3").text()).toBe("Test Article 1");
  });
});
