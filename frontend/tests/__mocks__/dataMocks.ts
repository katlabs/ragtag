export default {
  mock_articles: [
    {
      id: 1,
      link: {
        id: 1,
        tags: [
          {
            id: 1,
            word: "testtag1",
          },
        ],
        toots: [
          {
            id: 1,
            url: "https://example.com/testtoot1",
          },
        ],
        created: "2023-10-03T21:00:27.836144Z",
        last_modified: "2023-10-03T21:00:27.836189Z",
        url: "https://example.com/testarticle1",
      },
      title: "Test Article 1",
      authors: ["Test Author 1"],
      publish_date: "2023-09-20T04:09:56Z",
      fetch_date: "2023-10-03T21:00:31.402714Z",
      text: "This is the test article 1 text.",
      summary: "Test article 1 summary",
      keywords: ["test1", "keyword1"],
    },
    {
      id: 2,
      link: {
        id: 2,
        tags: [
          {
            id: 2,
            word: "testtag2",
          },
        ],
        toots: [
          {
            id: 2,
            url: "https://example.com/testtoot2",
          },
        ],
        created: "2023-10-03T21:00:27.836144Z",
        last_modified: "2023-10-03T21:00:27.836189Z",
        url: "https://example.com/testarticle2",
      },
      title: "Test Article 2",
      authors: ["Test Author 2"],
      publish_date: "2023-09-20T04:09:56Z",
      fetch_date: "2023-10-03T21:00:31.402714Z",
      text: "This is the test article 2 text.",
      summary: "Test article 2 summary",
      keywords: ["test2", "keyword2"],
    },
  ],
};
