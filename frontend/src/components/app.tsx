import { h } from "preact";
import { Route, Router } from "preact-router";

import Header from "./header";

// Code-splitting is automated for `routes` directory
//import Home from "../routes/home";
import News from "../routes/news";

const App = () => (
  <div id="app">
    <Header />
    <main>
      <Router>
        <Route path="/" component={News} />
        <Route path="/news/:id?" component={News} />
      </Router>
    </main>
  </div>
);

export default App;
