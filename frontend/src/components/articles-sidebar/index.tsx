import { h } from "preact";
import { Link } from "preact-router/match";
import style from "./style.scss";
import { getDateFormatted, getDateFromNow } from "src/utils";

const ArticlesSidebar = (props: any) => {
  const getArticleSource = (url: string) => {
    let domain = new URL(url);
    return domain.hostname;
  };
  const onChangeLimit = (e: any) => {
    props.setLimit(parseInt(e.target.value));
  };
  const getOptions = () => {
    const option_values = [10, 50, 100, 500, 1000, 5000];
    let options = option_values.filter(
      (x: number) => x == 10 || x <= props.total
    );
    options.push(props.total);
    return options;
  };
  return (
    <div class={style.sidebar}>
      {props.limit && (
        <div class={style.limit}>
          <span>Article Limit:</span>
          <select
            id="limit-dropdown"
            value={props.limit.toString()}
            onChange={onChangeLimit}
          >
            {getOptions().map((optionVal: number, index: number, arr: []) => (
              <option value={optionVal}>
                {index == arr.length - 1 ? `All (${optionVal})` : optionVal}
              </option>
            ))}
          </select>
        </div>
      )}
      <nav id="sidebar-nav">
        {props.articles.map((article: any) => (
          <Link activeClassName={style.active} href={`/news/${article.id}`}>
            <div class={style.title}>{article.title}</div>
            <div class={style.source}>{getArticleSource(article.link.url)}</div>
            <div class={style.time}>
              <div class={style.relativedate}>
                Published:&nbsp;
                <em>
                  {article.publish_date && getDateFromNow(article.publish_date)}
                </em>
              </div>
              <div class={style.relativedate}>
                Fetched:&nbsp;
                <em>
                  {article.fetch_date && getDateFromNow(article.fetch_date)}
                </em>
              </div>
              <div class={style.timeTooltip}>
                fetched:&nbsp;{getDateFormatted(article.fetch_date)}
                <br />
                published:&nbsp;{getDateFormatted(article.publish_date)}
              </div>
            </div>
          </Link>
        ))}
      </nav>
    </div>
  );
};

export default ArticlesSidebar;
