import { h } from "preact";
import { Link } from "preact-router/match";
import style from "./style.scss";

const Header = ({ ...props }) => {
  return (
    <header class={style.header}>
      <Link href="/">
        <img src="../../assets/RagTagTextLogo.png" width="300" />
      </Link>
      <h2 id="header-text">
        <span>Community-Curated News from the Fediverse</span>
      </h2>
      {/* <nav class={style.menubar}> */}
      {/*   <Link activeClassName={style.active} href="/"> */}
      {/*     Home */}
      {/*   </Link> */}
      {/*   <Link activeClassName={style.active} href="/news/:id?"> */}
      {/*     News */}
      {/*   </Link> */}
      {/* </nav> */}
    </header>
  );
};

export default Header;
