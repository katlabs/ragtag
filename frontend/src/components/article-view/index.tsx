import { h } from "preact";
import style from "./style.scss";
import { getDateFormatted, getDateFromNow } from "src/utils";

const ArticleView = (props: any) => {
  if (!props.article) {
    return <div class={style.article}></div>;
  }

  return (
    <div class={style.articleview}>
      <div class={style.article}>
        <h3 class={style.title}>{props.article.title}</h3>
        <div class={style.subtitle}>
          <a class={style.sourcelink} href={props.article.link.url}>
            &#128279;Article Source
          </a>
          <div
            class={style.time}
            publish-date={getDateFormatted(props.article.publish_date)}
            fetch-date={getDateFormatted(props.article.fetch_date)}
          >
            published:&nbsp;{getDateFromNow(props.article.publish_date)}
          </div>
          {props.article.authors.length > 0 && (
            <div class={style.authors}>
              by&nbsp;{props.article.authors.join(", ")}
            </div>
          )}
          {props.article.keywords.length > 0 && (
            <div class={style.keywords}>
              article&nbsp;keywords:&nbsp;{props.article.keywords.join(", ")}
            </div>
          )}
          {props.article.link.tags.length > 0 && (
            <div class={style.tags}>
              fediverse&nbsp;tags:&nbsp;
              {props.article.link.tags.map((t: any) => "#" + t.word).join(", ")}
            </div>
          )}
        </div>
        <div class={style.text}>{props.article.text}</div>
      </div>
    </div>
  );
};

export default ArticleView;
