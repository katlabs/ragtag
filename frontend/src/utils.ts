import moment from "moment";

export function getDateFormatted(date: any) {
  if (date == null) {
    return "unknown";
  } else return moment(date).format("MMM D, YYYY");
}

export function getDateFromNow(date: any) {
  if (date == null) {
    return "unknown";
  } else return moment(date).fromNow();
}
