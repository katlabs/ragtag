import { h } from "preact";
import { useEffect, useState } from "preact/hooks";
import style from "./style.scss";
import ArticleView from "../../components/article-view";
import ArticlesSidebar from "../../components/articles-sidebar";
import { route } from "preact-router";

const News = ({ ...props }) => {
  const [data, setData] = useState([]);
  const [limit, setLimit] = useState(10);
  const [count, setCount] = useState(0);

  useEffect(() => {
    fetch(`/api/articles/list/?limit=${limit}&offset=0`)
      .then((response) => response.json())
      .then((d) => {
        setData(d.results);
        setCount(d.count);
        if (d.results[0]) route("/news/" + d.results[0].id);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [limit]);

  const getCurrentArticle = () => {
    if (props.id == ":id") route("/news");
    if (props.id == "") return data[0];
    let article = data.find((article) => article.id == props.id) || data[0];
    return article;
  };

  return (
    <div class={style.news}>
      {data.length > 0 && (
        <ArticlesSidebar
          articles={data}
          total={count}
          limit={limit}
          setLimit={setLimit}
        />
      )}
      {data.length > 0 ? (
        <ArticleView article={getCurrentArticle()} />
      ) : (
        <div id="loader" class={style.noarticles}>
          Loading articles...
        </div>
      )}
    </div>
  );
};

export default News;
