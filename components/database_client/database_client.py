from sys import stdout
from typing import Callable, Dict
import requests
import json
import logging
import time


class DatabaseClient:
    session = requests.session()

    def __init__(
        self,
        base_url: str,
        username: str,
        password: str,
        retries: int = 5,
        interval_secs: int = 30,
    ) -> None:
        self.__setup_logger()
        self.base_url = base_url
        self.retries = retries
        self.interval_secs = interval_secs
        self.session.auth = (username, password)
        self.session.headers.update({"content-type": "application/json"})

    def __setup_logger(self):
        self.logger = logging.getLogger("database_client")
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            "%(asctime)s | %(name)s | %(levelname)s: %(message)s"
        )

        consolehandler = logging.StreamHandler(stdout)
        consolehandler.setFormatter(formatter)
        self.logger.addHandler(consolehandler)

    def __request_with_retry(
        self,
        url: str,
        request_function: Callable,
        errorstr: str,
        data: str | None = None,
    ) -> Dict | None:
        """
        Private function that makes a request and retries a set number of times
        """
        retry_times = self.retries
        if request_function == self.session.post:
            success_status = 201
        else:
            success_status = 200

        while retry_times >= 0:
            try:
                if data:  # Include post / put data
                    response = request_function(url, data=data)
                else:
                    response = request_function(url)
            except requests.exceptions.ConnectionError as err:
                self.logger.error(err)
            else:
                if response.status_code == success_status:
                    return response.json()
                if response.status_code == 404:
                    return None
                self.logger.error(f"{errorstr} status: {response.status_code}")

            retry_times = retry_times - 1
            if retry_times >= 0:
                self.logger.debug(
                    f"Retry {self.retries-retry_times} of {self.retries}"
                )
                time.sleep(self.interval_secs)

        raise Exception(f"API query retries exceeded")

    ### List requests
    def list_servers(self):
        url = f"{self.base_url}/servers/"
        errorstr = f"API get could not get servers list-"
        return self.__request_with_retry(url, self.session.get, errorstr)

    def list_articles(self):
        url = f"{self.base_url}/articles/list"
        errorstr = f"API get could not get articles list-"
        return self.__request_with_retry(url, self.session.get, errorstr)

    def list_links(self):
        url = f"{self.base_url}/links/"
        errorstr = f"API get could not get links list-"
        return self.__request_with_retry(url, self.session.get, errorstr)

    ### Get item requests
    def get_server(self, url):
        url = f"{self.base_url}/server/{url}/"
        errorstr = f"API get could not retrieve server {url}-"
        return self.__request_with_retry(url, self.session.get, errorstr)

    def get_article(self, id):
        url = f"{self.base_url}/article/{id}/"
        errorstr = f"API get could not retrieve article {id}-"
        return self.__request_with_retry(url, self.session.get, errorstr)

    def get_link(self, url):
        url = f"{self.base_url}/link/{url}/"
        errorstr = f"API get could not retrieve link {url}-"
        return self.__request_with_retry(url, self.session.get, errorstr)

    ### Update item requests
    def update_server(self, url, server):
        url = f"{self.base_url}/server/{url}/"
        errorstr = f"API put could not update server {url}-"
        return self.__request_with_retry(
            url, self.session.put, errorstr, json.dumps(server)
        )

    def update_article(self, id, article):
        url = f"{self.base_url}/article/{id}/"
        errorstr = f"API put could not update article {id}-"
        return self.__request_with_retry(
            url, self.session.put, errorstr, json.dumps(article)
        )

    def update_link(self, url, link):
        url = f"{self.base_url}/link/{url}/"
        errorstr = f"API put could not update link {url}-"
        return self.__request_with_retry(
            url, self.session.put, errorstr, json.dumps(link)
        )

    ### Create item requests
    def create_server(self, server):
        url = f"{self.base_url}/servers/"
        errorstr = f"API post could not create server-"
        return self.__request_with_retry(
            url, self.session.post, errorstr, json.dumps(server)
        )

    def create_article(self, article):
        url = f"{self.base_url}/articles/"
        errorstr = f"API post could not create article-"
        return self.__request_with_retry(
            url, self.session.post, errorstr, json.dumps(article)
        )

    def create_link(self, link):
        url = f"{self.base_url}/links/"
        errorstr = f"API post could not create link-"
        return self.__request_with_retry(
            url, self.session.post, errorstr, json.dumps(link)
        )
