import pytest
import unittest
from playwright.sync_api import Page, expect
import re


class NewsTests(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def setup(self, page: Page):
        self.page = page
        self.page.goto("/")

    def test_homepage_has_header(self):
        expect(self.page.locator("#header-text")).to_be_visible()

    def test_article_text_is_visible(self):
        article = self.page.locator("main>div>div").nth(1).locator("div").nth(0)
        expect(article).to_have_class(re.compile(r"article"))
        article_text = article.locator(">div").nth(1)
        expect(article_text).to_have_class(re.compile(r"text"))
        expect(article_text).to_be_visible()
        expect(article_text).to_contain_text(re.compile(r".*"))

    def test_sidebar_is_visible(self):
        sidebar = self.page.locator("main>div").nth(0).locator("div").nth(0)
        expect(sidebar).to_have_class(re.compile(r"sidebar"))
        expect(sidebar).to_be_visible()
        expect(sidebar.locator("#sidebar-nav")).to_be_visible()

    def test_first_link_is_active(self):
        first_article_link = self.page.locator("#sidebar-nav>a").nth(0)
        expect(first_article_link).to_be_visible
        expect(first_article_link).to_have_class(re.compile(r"active"))

    def test_links_have_correct_children(self):
        links = self.page.locator("#sidebar-nav>a").all()
        for link in links:
            title = link.locator("div").nth(0)
            expect(title).to_be_visible()
            expect(title).to_have_class(re.compile(r"title"))
            expect(title).to_have_text(re.compile(r".*"))
            source = link.locator("div").nth(1)
            expect(source).to_be_visible()
            expect(source).to_have_class(re.compile(r"source"))
            time = link.locator("div").nth(2)
            expect(time).to_be_visible()
            expect(time).to_have_class(re.compile(r"time"))
            expect(time.locator("div")).to_have_count(3)

    def test_navigate_and_check_news(self):
        article_links = self.page.locator("#sidebar-nav>a")
        if len(article_links.all()) < 2:
            pytest.skip("Not enough articles to test navigation.")
        second_article_link = article_links.nth(1)
        second_article_title = (
            second_article_link.locator("div").nth(0).inner_html()
        )
        second_article_link.click()
        expect(self.page.locator("h3")).to_have_text(second_article_title)
