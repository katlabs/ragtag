# RagTag

RagTag is a web application that retrieves news articles that are linked in posts with trending tags in popular fediverse instances. It is a project for a software architecture course and is in development. It is currently [publicly available](https://ragtag.katlabs.cc). The source code is hosted on [GitLab](https://gitlab.com/katlabs/ragtag/).

## Running via Docker

Docker must be installed to run RagTag locally. To run the development version, enter the following in the project root:

```shell
cp .env.example .env.dev
docker compose -f docker-compose.dev.yml up --build -d
```

The frontend will be available at http://localhost:8783. The whole stack may take some time to come up, and the news articles will not be available on the frontend until the datacollector and the dataanalyzer are finished. The scaled applications (datacollector and dataanalyzer) will have errors when docker compose tries to pull some of their instances because they rely on the initial instance to perform the build step. These errors can be ignored.

## Datacollector

The datacollector is scalable and collects links from toots with trending tags from one fediverse instance. The development version, as a proof of concept, has two datacollector services in docker-compose.dev.yml. Each service queries the backend api to check if each link is in the database, then either creates or updates the link. The progress of the datacollectors can be seen in the docker compose logs:

```shell
docker compose -f docker-compose.dev.yml logs -f | grep ragtag-datacollector
```

The datacollector runs once on container start-up, then it runs every 30 minutes via cron.

## Dataanalyzer

The dataanalyzer is also scalable and receives messages (via rabbitmq) when the datacollector posts a new link. The message queue is dispatched round robin, and when the dataanalyzer receives the message that contains a url, it retrieves the link data and uses the url to scrape the article with the [newspaper3k library](https://github.com/codelucas/newspaper). If the result is a valid article, it posts the article data to the backend api. In the development version, there are two dataanalyzers The progress of the dataanalyzers can be seen in the docker compose logs:

```shell
docker compose -f docker-compose.dev.yml logs -f | grep ragtag-dataanalyzer
```

## Databaseclient

The database client is a shared component between datacollector and dataanalyzer. It provides a simple interface to the backend API to use the database. It can be found in components/database_client.

## Testing

### Python Tests

Python-based unit tests, and integration tests are done with pytest, with the playwright plugin, so python3 is required. To run the tests, first run the shell commands in the section above, then run the following commands in the project root:

```shell
python -m venv venv
source venv/bin/activate
pip install -r backend/requirements.txt
playwright install
pytest --cov=backend/ --cov-config=backend/.coveragerc
```

If there are not enough articles fetched, some of the integration tests may fail or be skipped. This is a limitation of the development environment using only two servers that may not have many news article links at a given time. However, all of the unit tests should succeed, as they use a test database with mock data.

### Javascript Tests

Front end unit tests use jest and Enzyme. Node/npm is required to run the tests.

```shell
cd frontend
npm test -- --coverage
cd ..
```

## Health, Metrics & Monitoring

### Health Check

In development, a health check endpoint is available at http://localhost:8783/health/.

### API Metrics

Backend API requests are monitored with [Prometheus](https://github.com/prometheus/prometheus) via the [django-prometheus library](https://github.com/korfuri/django-prometheus). The metrics are made available at http://localhost:8782/metrics for use by Prometheus. For example, the following curl request will show the total GET and POST requests made to the backend:

```shell
curl http://localhost:8782/metrics | grep django_http_requests_total_by_method_total
```

### Nginx Metrics

Overall requests metrics use nginx [stub_status](http://nginx.org/en/docs/http/ngx_http_stub_status_module.html) and [nginx-prometheus-exporter](https://github.com/nginxinc/nginx-prometheus-exporter) to create a metrics endpoint at http://localhost:9113/metrics for use by Prometheus. For example, the following curl request will show the total http requests received:

```shell
curl http://localhost:9113/metrics | grep nginx_http_requests_total
```

### Monitoring with Prometheus

Prometheus itself is available at http://localhost:9090. For example, you can see [a graph of backend requests](http://localhost:9090/graph?g0.range_input=1h&g0.stacked=1&g0.expr=django_http_requests_total_by_method_total&g0.tab=0) or [a graph of nginx requests](http://localhost:9090/graph?g0.expr=nginx_http_requests_total&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h).

### Visualization with Grafana

Grafana is available at http://localhost:3000. The default development username and password are _admin_ and _password_, and can be edited in the file grafana/security.ini. Within the grafana UI, one can create a dashboard with the prometheus datasource, which is automatically connected to grafana in the development docker build.

## Stopping via Docker

To end the application, put down the docker container with docker compose and specify the development docker compose file:

```shell
docker compose -f docker-compose.dev.yml down
```

## CI/CD

RagTag uses GitLab for Continuous Integration and Continuous Deployment. The file .gitlab-ci.yml contains the configuration for the testing and deployment pipelines.

On a normal push (without a release tag) the pipeline includes unit testing, building and pushing docker containers to the GitLab registry.

On a push tagged as a release, the docker containers are tagged for release in the registry, then they are deployed to a cluster on Google Kubernetes Engine. Once deployed, integration tests are performed on the live application. If the integration tests fail, the application is rolled back to the previous passing deployment. A destroy job is made available in the pipeline for manual use in order to easily destroy the cluster.
