from datetime import datetime
import logging
import logging.handlers
import os
from sys import stdout
from urllib.parse import quote
from database_client import DatabaseClient
from .config import config
from pika import BlockingConnection, ConnectionParameters, PlainCredentials
from newspaper import Article, Config


class Dataanalyzer:
    def __init__(self) -> None:
        self.__setup_logger()
        self.logger.info(f"Dataanalyzer started")
        self.__setup_database()
        self.__setup_rabbitmq()

    def __setup_logger(self):
        self.logger = logging.getLogger(config.app_name)
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            "%(asctime)s | %(name)s | %(levelname)s: %(message)s"
        )

        consolehandler = logging.StreamHandler(stdout)
        consolehandler.setFormatter(formatter)
        self.logger.addHandler(consolehandler)

    def __setup_database(self):
        api_password = os.getenv("DATAANALYZER_PASSWORD", None)
        if not api_password:
            raise Exception("No api password provided in environment.")

        self.client = DatabaseClient(
            config.api_base_url, config.app_name, api_password
        )

    def __setup_rabbitmq(self):
        rabbit_user = os.getenv("RABBITMQ_DEFAULT_USER", None)
        if not rabbit_user:
            raise Exception("No rabbitmq password provided in environment.")
        rabbit_pw = os.getenv("RABBITMQ_DEFAULT_PASS", None)
        if not rabbit_pw:
            raise Exception("No rabbitmq password provided in environment.")
        rq_credentials = PlainCredentials(rabbit_user, rabbit_pw)
        rq_con_params = ConnectionParameters(
            host="rabbitmq", credentials=rq_credentials
        )
        try:
            rq_connection = BlockingConnection(rq_con_params)
            rq_channel = rq_connection.channel()
        except:
            raise Exception(f"Dataanalyzer failed to connect to rabbitmq.")

        rq_channel.queue_declare("links")

        rq_channel.basic_consume(
            queue="links",
            on_message_callback=self.__link_callback,
            auto_ack=True,
        )

        rq_channel.start_consuming()

    def __link_callback(self, channel, method_frame, header_frame, body):
        self.logger.info(f"Got Link from queue: {body}")
        link_url = body.decode()
        link_url_safe = quote(link_url, safe="")
        self.logger.info(f"Link: {link_url}, safe url: {link_url_safe}")
        db_link = self.client.get_link(link_url_safe)
        if not db_link:
            self.logger.error(
                f"Could not retrieve link {link_url} from the database, skipping..."
            )
            return
        self.logger.debug(f"Processing link {link_url}...")
        article_data = self.scrape_article(link_url)
        if not article_data:
            self.logger.debug(f"Could not get article, skipping...")
        else:
            article_data["link"] = db_link["id"]
            new_article = self.client.create_article(article_data)
            if not new_article:
                self.logger.error(f"Article not created from link {link_url}")
            else:
                self.logger.debug(f"Article created: {new_article['id']}")

    def scrape_article(self, url):
        try:
            article = Article(
                url=url,
                fetch_images=False,
                language="en",
                MAX_SUMMARY=500,
                MIN_WORD_COUNT=500,
            )
            article.download()
            article.parse()
            if (not article.is_valid_body()) or (
                len(article.authors) == 0 and article.publish_date is None
            ):
                return None
            article.nlp()
            publish_date = (
                article.publish_date.isoformat()
                if isinstance(article.publish_date, datetime)
                else article.publish_date
            )
            article_data = {
                "title": article.title,
                "authors": article.authors,
                "publish_date": publish_date,
                "text": article.text,
                "summary": article.summary,
                "keywords": article.keywords,
            }
            return article_data
        except:
            return None


if __name__ == "__main__":
    Dataanalyzer()
